// -----------------------------------------------------------
// Фиксированные html-элементы, присутсвтуюшие на странице, именованны caps`ом

const QUESTIONS_CONTAINER = document.querySelector(".questions-container");
const QUESTION_TEXT = document.getElementById("question-text");
const ANSWERS_LIST = document.querySelector(".answers-block");
// -----------------------------------------------------------
const PREVIOUS_QUESTION_BUTTON = document.getElementById(
  "previous-question-button"
);

const NEXT_QUESTION_BUTTON = document.getElementById("next-question-button");
// -----------------------------------------------------------
const RESULTS_WINDOW = document.querySelector(".results-window");
RESULTS_WINDOW.style.display = "none";

const RESULTS_BUTTON = document.getElementById("get-results-button");
const SCORE_ELEMENT = document.getElementById("score");
const NEW_RUN_BUTTLE = document.getElementById("new-run-button");

// -----------------------------------------------------------

// для интереса перемешаем вопросы
// функция перемешивания взята со stackoverflow:
//  https://stackoverflow.com/questions/2450954/how-to-randomize-shuffle-a-javascript-array
function makeShuffledArray(array) {
  for (let i = array.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [array[i], array[j]] = [array[j], array[i]];
  }

  return array;
}

// -----------------------------------------------------------
// Вспомогательные классы
// Класс содержит методы для отрисовки вопросов
// названия методов, начинающиеся с _, подразумевают их приватность
class Quiz {
  // текущие ответы будут сохраняться при возращении к вопросу
  currentAnswers = undefined;

  constructor(questionsList) {
    this.currentQuestionIndex = 0;
    this.questionsCount = questionsList.length;
    this.lastQuestionIndex = this.questionsCount - 1;

    this.questionsUnsuffledList = questionsList;
    this.questionsList = questionsList;

    this._initNavigationButtons();
    this.run();
  }

  // вернуть квиз в начальное состояние и запустить с первого вопроса
  run(shuffleQuestions = true) {
    if (shuffleQuestions) {
      this.questionsList = makeShuffledArray([...this.questionsUnsuffledList]);
    }

    this.currentQuestionIndex = 0;
    this.currentAnswers = Array(this.questionsCount).fill(null);

    QUESTIONS_CONTAINER.style.display = "block";
    RESULTS_WINDOW.style.display = "none";
    this._setNavigationButtonsDisableState();
    this._renderCurrentQuestion();
  }

  // вспомогательный метод для получения текущего вопроса и его индекса
  _getCurrentQuestion() {
    return {
      questionIndex: this.currentQuestionIndex,
      questionObj: this.questionsList[this.currentQuestionIndex],
    };
  }

  // показать окно с результатом, скрыть элемент с вопросом
  _showResults() {
    let score = 0;
    for (let i = 0; i < this.questionsCount; ++i) {
      const currentAnswer = this.currentAnswers[i];
      const rightAnswer = this.questionsList[i].rightAnswerIndex;
      if (currentAnswer === rightAnswer) {
        console.log(`Question ${i}: ${currentAnswer} = ${rightAnswer}`);
        score++;
      } else {
        console.log(`Question ${i}: ${currentAnswer} != ${rightAnswer}`);
      }
    }
    SCORE_ELEMENT.textContent = `Ваш результат: ${score}/${this.questionsCount}`;

    QUESTIONS_CONTAINER.style.display = "none";
    RESULTS_WINDOW.style.display = "block";
  }

  // отрисовка блока с вопросом
  _renderCurrentQuestion() {
    const currentQuestion = this.questionsList[this.currentQuestionIndex];
    QUESTION_TEXT.textContent = currentQuestion.questionText;

    this._renderAnswers(currentQuestion);
  }

  // отрисовка блока с варинтами ответа
  _renderAnswers() {
    ANSWERS_LIST.innerHTML = "";

    const { questionIndex, questionObj } = this._getCurrentQuestion();
    const { answersList } = questionObj;
    answersList.forEach((answerText, answerIndex) => {
      const asnwerElement = document.createElement("div");
      asnwerElement.classList.add("answer");

      const radioButton = document.createElement("input");
      radioButton.setAttribute("type", "radio");
      radioButton.setAttribute("name", "answer");
      const buttonID = `answer-${questionIndex}-${answerIndex}`;
      radioButton.setAttribute("id", buttonID);
      if (this.currentAnswers[questionIndex] === answerIndex) {
        radioButton.setAttribute("checked", buttonID);
      }
      radioButton.addEventListener(
        "click",
        function () {
          this.currentAnswers[questionIndex] = answerIndex;
        }.bind(this)
      );
      asnwerElement.appendChild(radioButton);

      const answerLabel = document.createElement("label");
      answerLabel.setAttribute("for", buttonID);
      answerLabel.textContent = answerText;
      asnwerElement.appendChild(answerLabel);

      ANSWERS_LIST.appendChild(asnwerElement);
    });
  }

  // если вопрос первый или последний, то одна из кнопок перехода к соседнему вопросу должна быть выключена
  _setNavigationButtonsDisableState() {
    if (this.currentQuestionIndex === 0) {
      PREVIOUS_QUESTION_BUTTON.disabled = true;
      NEXT_QUESTION_BUTTON.disabled = false;
    } else if (this.currentQuestionIndex === this.lastQuestionIndex) {
      PREVIOUS_QUESTION_BUTTON.disabled = false;
      NEXT_QUESTION_BUTTON.disabled = true;
    } else {
      PREVIOUS_QUESTION_BUTTON.disabled = false;
      NEXT_QUESTION_BUTTON.disabled = false;
    }
  }

  // вспомогательная функция для назначения listener`ов на кнопки
  _initNavigationButtons() {
    PREVIOUS_QUESTION_BUTTON.addEventListener(
      "click",
      function () {
        if (this.currentQuestionIndex > 0) {
          this.currentQuestionIndex--;
        }
        this._renderCurrentQuestion();
        this._setNavigationButtonsDisableState();
      }.bind(this)
    );

    NEXT_QUESTION_BUTTON.addEventListener(
      "click",
      function () {
        if (this.currentQuestionIndex < this.lastQuestionIndex) {
          this.currentQuestionIndex++;
        }
        this._renderCurrentQuestion();
        this._setNavigationButtonsDisableState();
      }.bind(this)
    );

    RESULTS_BUTTON.addEventListener(
      "click",
      function () {
        this._showResults();
      }.bind(this)
    );

    NEW_RUN_BUTTLE.addEventListener(
      "click",
      function () {
        this.run();
      }.bind(this)
    );
  }
}

// Класс содержит данные для отдельного вопроса:
//    текст вопроса, варианты ответов, номер правильного ответа
class QuizQuestion {
  constructor(initData) {
    this.questionText = initData.questionText;
    this.answersList = initData.answersList;
    this.rightAnswerIndex = initData.rightAnswerIndex;
  }
}

// -----------------------------------------------------------
// Список вопросов для викторины взят с
//  https://kupidonia.ru/viktoriny-play/viktorina-udivitelnyj-mir-kosmosa

const QUESTIONS_LIST = [
  {
    questionText: "Сколько планет в Солнечной системе?",
    answersList: [7, 8, 9],
    rightAnswerIndex: 1,
  },
  {
    questionText: "Звезды какого цвета не существует?",
    answersList: ["Синяя", "Зеленая", "Жёлтая", "Красная"],
    rightAnswerIndex: 1,
  },
  {
    questionText: "Что такое Супернова (Supernova)?",
    answersList: [
      "Сверхновая звезда (вспышка сверхновой), возникающая в конце эволюции звёзд",
      "Недавно обнаруженная планет-близнец Земли",
      "Только что открытая комета",
    ],
    rightAnswerIndex: 0,
  },
  {
    questionText: "Что такое «Солнечный ветер»?",
    answersList: [
      "Поток супер-ионизированных частиц из солнечной короны",
      "Взрывной процесс выделения энергии в атмосфере Солнца",
      "Внешние слои атмосферы Солнца",
    ],
    rightAnswerIndex: 0,
  },
  {
    questionText:
      "Какая страна первой запустила в космос искусственный спутник Земли?",
    answersList: ["США", "Китай", "CCCР"],
    rightAnswerIndex: 2,
  },
  {
    questionText: "Спутником какой планеты является Каллисто?",
    answersList: ["Венера", "Юпитер", "Марс", "Нептун"],
    rightAnswerIndex: 1,
  },
  {
    questionText: "Что не является галактикой?",
    answersList: [
      "Андромеда",
      "Магелланово Облако",
      "Альдебаран",
      "Млечный Путь",
    ],
    rightAnswerIndex: 2,
  },
  {
    questionText: "В какой галактике находится Земля?",
    answersList: ["Млечный Путь", "Андромеда", "Магелланово Облако"],
    rightAnswerIndex: 0,
  },
  {
    questionText:
      "Звёздами какого созвездия являются Сегин, Рукбах, Нави, Шедар и Каф?",
    answersList: ["Большая Медведица", "Кассиопея", "Гончие Псы"],
    rightAnswerIndex: 1,
  },
  {
    questionText: "Экзопланета — это планета ... ",
    answersList: [
      "Находящаяся вне Солнечной системы",
      "Находящаяся на краю Солнечной системы",
      "Находящаяся в центре Солнечной системы",
    ],
    rightAnswerIndex: 0,
  },
].map((questionData) => new QuizQuestion(questionData));

// -----------------------------------------------------------

document.addEventListener("DOMContentLoaded", () => {
  const quiz = new Quiz(QUESTIONS_LIST);
});
